package com.ibpd.henuocms.service.nodeAttr;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.NodeAttrEntity;

public interface INodeAttrService extends IBaseService<NodeAttrEntity> {
	NodeAttrEntity getNodeAttr(Long nodeId);
}
 