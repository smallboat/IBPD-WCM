package com.ibpd.henuocms.service.content;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.assist.ExtContentEntity;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.ext.ContentExtEntity;

public interface IContentService extends IBaseService<ContentEntity> {

	List<ContentExtEntity> getContentList(Long currentNodeId,Integer currentPageSize,Integer currentPageIndex,String orderField,String orderType);
	List<ContentEntity> getContentListByNodeId(Long nodeId,Integer pageSize,Integer pageIndex,String orderField,String orderType);
	ContentExtEntity getContentExtEntity(Long contentId);
	List<ExtContentEntity> getContentList(Long currentSiteId, Long currentNodeId,
			Integer currentPageSize, Integer currentPageIndex,String filter,String orderType);
}  
 