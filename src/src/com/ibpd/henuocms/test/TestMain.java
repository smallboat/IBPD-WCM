package com.ibpd.henuocms.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael
 *
 */
public class TestMain {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        TestMain test = new TestMain();
        FormTable fromTable = test.initData();

        TableGenerator tg = new TableGenerator(fromTable);
        tg.generatorTable();

    }

    /**
     * ��ʼ������
     * @return
     */
    private FormTable initData() { 
        FormTable form = new FormTable();
        form.setName("testTable");
        form.setTableName("TB_GEN");

        List<ColumnAttribute> list = new ArrayList<ColumnAttribute>();
        ColumnAttribute attr = new ColumnAttribute();
        attr.setName("collectKey");
        attr.setColumnType("string");
        attr.setColumnName("COLLECTKEY");
        attr.setLength(100);
        list.add(attr);
        ColumnAttribute attr1 = new ColumnAttribute();
        attr1.setName("mibVal");
        attr1.setColumnType("string");
        attr1.setColumnName("MIBVAL");
        attr1.setLength(100);
        list.add(attr1);
        ColumnAttribute attr2 = new ColumnAttribute();
        attr2.setName("dsname");
        attr2.setColumnType("string");
        attr2.setColumnName("DSNAME");
        attr2.setLength(100);
        list.add(attr2);
        ColumnAttribute attr3 = new ColumnAttribute();
        attr3.setName("timestamp");
        attr3.setColumnType("long");
        attr3.setColumnName("TIMESTAMP");
        list.add(attr3);
        form.setFormAttributeList(list);
        return form;
    }

}