package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.Pinyin4jUtil;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.nodeForm.INodeFormService;
import com.ibpd.henuocms.service.nodeForm.NodeFormServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;

@Controller
public class Sys extends BaseController {
	@RequestMapping("Manage/Sys/index.do")
	public String index(org.springframework.ui.Model model,HttpServletRequest req) throws IOException{
		model.addAttribute(PAGE_TITLE,"参数设置");
		return "manage/sys/index";
	}	

}
