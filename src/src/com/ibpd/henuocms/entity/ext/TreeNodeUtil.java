package com.ibpd.henuocms.entity.ext;

import java.util.ArrayList;
import java.util.List;

import com.ibpd.entity.baseEntity.IBaseEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.shopping.entity.ProductEntity;
/**
 * 将普通的node实体对象 转换为easyui专用的node对象
 * @author mg by qq:349070443
 *
 */ 
public class TreeNodeUtil {
	/**
	 * 将普通的node实体对象 转换为easyui专用的node对象
	 * @param nodeList
	 */
	public static List<TreeNodeEntity> converToTreeNode(List list){
		List<TreeNodeEntity> tnList=new ArrayList<TreeNodeEntity>();
		if(list==null)
			return tnList;
		else{
			for(Object n:list){
				if(n instanceof NodeEntity){
					NodeEntity ne=(NodeEntity) n;
					tnList.add(new TreeNodeEntity(ne.getId().toString(),ne.getText(),(ne.getLeaf())?"closed":"open",(ne.getLeaf())?"icon-folder":"icon-file","false"));					
				}else if(n instanceof ProductEntity){
					ProductEntity ne=(ProductEntity) n;
					tnList.add(new TreeNodeEntity(ne.getId().toString(),ne.getName(),"open","icon-file","false"));					
				}
			}
		}
		return tnList;
	}
}
