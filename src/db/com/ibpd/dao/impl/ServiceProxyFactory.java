/*    */ package com.ibpd.dao.impl;
/*    */ 
/*    */ import javax.annotation.Resource;

import com.ibpd.dao.IDao;
import com.ibpd.henuocms.web.controller.manage.BaseController;
/*    */ 
/*    */ public class ServiceProxyFactory
/*    */ {
	@Resource(name="daoImpl")
	private IDao daoImpl;
/*    */   public static Object getServiceProxy(Class serviceImplclass)
/*    */   { 
///* 20 */     Object retObj = null;
///* 21 */     Object delegate = null;
///*    */     try
///*    */     {
///* 24 */       delegate = serviceImplclass.newInstance();
///* 25 */       Method mh = delegate.getClass().getMethod("setDao", new Class[] { IDao.class });
///* 26 */       IDao dao = null;
///* 27 */       dao = DaoImpl.class.newInstance();
///* 28 */       mh.invoke(delegate, new Object[] { dao });
///* 29 */       ServiceProxy sih = new ServiceProxy(delegate);
///* 30 */       retObj = Proxy.newProxyInstance(serviceImplclass.getClassLoader(), serviceImplclass
///* 31 */         .getInterfaces(), sih);
///*    */     }
///*    */     catch (Exception e) {
///* 34 */       e.printStackTrace();
///*    */     }
///* 36 */     return retObj;
	BaseController bc=new BaseController();
	String s=serviceImplclass.getSimpleName();
	s=s.substring(0,1).toLowerCase()+s.substring(1);
	s=s.replace("Impl","");
	return bc.getBeanFromSpringDefinedBeans(s);
/*    */   }
/*    */
public void setDaoImpl(IDao daoImpl) {
	this.daoImpl = daoImpl;
}
public IDao getDaoImpl() {
	return daoImpl;
} }