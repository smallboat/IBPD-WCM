package com.ibpd.dao.impl;

import com.ibpd.dao.IDao;
import com.ibpd.entity.baseEntity.IBaseEntity;
import java.io.Serializable;
import java.util.List;

public abstract interface IBaseService<Entity extends IBaseEntity>
{
  public abstract Entity getEntityById(Long paramLong);

  public abstract List<Entity> getList();

  public abstract List<Entity> getList(String paramString1, List<HqlParameter> hqlParameterList,String paramString2, Integer paramInteger1, Integer paramInteger2);

  public abstract List<Entity> getList(String paramString,List<HqlParameter> hqlParameterList);

  public abstract List<Entity> getList(String paramString, Integer paramInteger1, Integer paramInteger2);

  public abstract int saveEntity(Entity paramEntity);

  public abstract int updateEntity(Entity paramEntity);

  public abstract int deleteByPK(Serializable paramSerializable);

  public abstract Long getRowCount(String paramString,List<HqlParameter> hqlParameterList);

  public abstract Boolean batchDelete(Long[] paramArrayOfLong);

  public abstract String getTableName();

  public abstract void setDao(IDao<Entity> paramIDao);

  public abstract IDao<Entity> getDao();

  public abstract int batchDel(String paramString);
}