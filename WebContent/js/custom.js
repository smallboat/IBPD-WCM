	/**
	*根据gridID获取dataGrid中选中行的ID
	**/
	function getSelections(gridId){
		var _sets=$("#"+gridId).datagrid("getSelections");
		var _ids="";
		$.each(_sets,function(i,n){
		_ids+=n.id+",";
		});
		return _ids;
	};
	/**
	*重新载入datagrid数据,并将选中状态全部取消
	*@param gridId datagrid的ID属性
	**/
	function reload(gridId){
		$("#"+gridId).datagrid("uncheckAll");
		$("#"+gridId).datagrid("reload");
	};
	/**
	*该函数用来为datagrid返回自定义的常规按钮
	*@param gridId datagrid的ID属性
	*@param btnId 为按钮设置的Id
	*@param btnText 为按钮设置的显示文字
	*@param iconCls 为按钮设置图标
	*@param fun 按钮点击应该执行的函数名称，目前不支持带参数的函数
	**/
	function genericToolButton(gridId,btnId,btnText,iconCls,fun){
		return {
			text:btnText,
			id:btnId,
			iconCls:iconCls,
			handler:function(){
				fun();
			}
		};
	};
	/**
	*为datagrid返回一个自定义的删除按钮
	*@param gridId datagrid的ID属性
	*@param postUrl 删除操作请求的url地址
	**/
	function delToolButton(gridId,postUrl){
		return {
			text:'删除',
			id:'btn_del',
			iconCls:'icon_remove',
			handler:function(){
				customDelByGrid(gridId,postUrl);
			}
		};
	};
	/**
	*为datagrid返回一个带有请求操作的工具栏按钮
	*@param gridId datagrid的id属性
	*@param postUrl 提交请求的地址
	*@param buttonId 为按钮定义ID属性
	*@param buttonIcon 为按钮定义图标
	*@param buttonText 为按钮定义显示的文字
	*@param enableConfirmMsg 在提交请求前是否需要确认提示
	*@param confirmText 确认提示的文字
	*@param successText 执行成功后的提示
	**/
	function PostDataByGridToolButton(gridId,postUrl,buttonId,buttonIcon,buttonText,enableConfirmMsg,confirmText,successText){
		return {
			text:buttonText,
			id:buttonId,
			iconCls:buttonIcon,
			handler:function(){
				customPostByGrid2(gridId,enableConfirmMsg,confirmText,postUrl,successText);
			}
		};
	};
	/**
	*为datagrid返回一个刷新按钮
	*@param gridId datagrid的id
	**/
	function reloadToolButton(gridId){
		return {
			text:"刷新",
			id:"btn_reload",
			iconCls:"btn-reload",
			handler:function(){
				reload(gridId);
			}
		};
	};
	/**
	*自定义删除请求
	*@param gridId
	*@param postUrl 删除请求的url地址
	**/
	function customDelByGrid(gridId,postUrl){
		customPostByGrid(gridId,"确定要删除吗?",postUrl,"删除成功");
	}
	/**
	*自定义的post请求
	*@param gridId
	*@param confirmText
	*@param postUrl
	*@param successText
	**/
	function customPostByGrid(gridId,confirmText,postUrl,successText){
		customPostByGrid2(gridId,true,confirmText,postUrl,successText);
	};
	function customPostByGrid2(gridId,enableConfirm,confirmText,postUrl,successText){
		var _ids=getSelections(gridId);
		if(_ids.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			if(enableConfirm){
				$.messager.confirm("确认",confirmText,function(r){
					if(r){
						$.post(
							postUrl,
							{ids:_ids},
							function(result){
								if(result.indexOf("msg")!=-1){
									var _o=eval("("+result+")");
									if(_o.status=='99'){
										msgShow("提示",successText,"warning");
										reload(gridId);
									}else{
										msgShow("提示",_o.msg,"warning");
									}
								}
								
							}
						);
					}
				});
			}else{
				$.post(
					postUrl,
					{ids:_ids},
					function(result){
						if(result.indexOf("msg")!=-1){
							var _o=eval("("+result+")");
							if(_o.status=='99'){
								msgShow("提示",successText,"warning");
								reload(gridId);
							}else{
								msgShow("提示",_o.msg,"warning");
							}
						}
							
					}
				);
			}
		}
	};

	var cmenu;
	function getcmenu(){
		return cment;
	}
	/**
	*加载属性页面
	*@propId
	*@url
	**/
	function loadProps(propId,url){
		if($("#"+propId).length>0){
			$("#"+propId).attr("src",url);
		}else{
			$("#"+propId,parent.document).attr("src",url);
		}
	};
	/**
	*根据dataGrid id创建该grid的表头菜单,其实就是所有列名称的显示与隐藏的功能菜单<br>
	*该函数存在一些样式上的bug，即无法自适应高度,最近抽时间解决
	*@param tableGridId dataGrid的id
	**/
    function createColumnMenu(tableGridId){
            cmenu = $('<div style="height:300px;overflow-y:scroll"/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#'+tableGridId).datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#'+tableGridId).datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#'+tableGridId).datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#'+tableGridId).datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
	/**
	*弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	**/
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	/**
	*关闭当前窗口的实现,兼容chrome、firefox、ie8,其他有待测试
	**/
	function closeCurWindow(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};
	 var oTime = null;
    function resize(propsPanelId)
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(function(){reset(propsPanelId);}, 200);
    }
     
    function reset(propsPanelId)
    {
        var frame = document.getElementById(propsPanelId);
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }


