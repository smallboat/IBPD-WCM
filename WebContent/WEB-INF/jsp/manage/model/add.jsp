<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <form id="fm" class="forms" action="<%=basePath%>manage/model/doAdd.do" method="post">
    	<input type="hidden" name="id" id="id" value=""/>
    	
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
    			<th class="title">内容模型名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="modelName" name="modelName" tip="输入验证模型名称" datatype="*3-10" errormsg="名称长度不得小于3且不得大于10"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">排序</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="order" name="order" tip="输入正整数“ datatype="n1-10"  errormsg="输入正整数"/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    		<tr>
    			<th class="title">说明</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="intro" name="intro" tip="说明文字" datatype="*1-100"  errormsg=""/></div>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></td>
    		</tr>
    	</table>
    	<input type="submit"/>&nbsp;&nbsp;<input type="button" value="关闭" onclick="javascript:$('#addDialog').window('close');"/>
    </form> 
    <script type="text/javascript">
    $(function(){
		
	$("#fm").Validform({
		tiptype:2,
		callback:function(form){
			var check=confirm("您确定要提交表单吗？");
			if(check){
				form[0].submit();
			}
			
			return false;
		}
		
	});
})
</script>