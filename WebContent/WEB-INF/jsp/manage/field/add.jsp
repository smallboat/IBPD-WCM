<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <form id="fm" class="forms" action="<%=basePath%>manage/field/doAdd.do" method="post">
    	<input type="hidden" name="id" id="id" value=""/>
    	<input type="hidden" name="modelId" id="modelId" value=""/>
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
    			<th class="title">字段显示名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="displayName" name="displayName"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">字段表单名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="formName" name="formName"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">字段类型</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="htmlType" name="htmlType"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">验证模型ID</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="validateModelId" name="validateModelId"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">排序</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="order" name="order"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">默认值</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="defaultValue" name="defaultValue"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">可选值</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="optionValue" name="optionValue"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">字段长度</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="fieldLength" name="fieldLength"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">字段说明</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="intro" name="intro"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">字段类型</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="fieldType" name="fieldType"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    	</table>
    	<input type="submit"/>&nbsp;&nbsp;<input type="button" value="关闭" onclick="javascript:$('#addDialog').window('close');"/>
    </form> 
    <script type="text/javascript">
    $(function(){
		
	$("#fm").Validform({
		tiptype:2,
		callback:function(form){
			var check=confirm("您确定要提交表单吗？");
			if(check){
				form[0].submit();
			}
			
			return false;
		}
		
	});
})
</script>